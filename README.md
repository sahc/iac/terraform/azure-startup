# azure-startup

Sample Terraform startup project with Azure/Proyecto de ejemplo para inicio de Terraform con Azure

**INTRODUCCI&Oacute;N**

La infraestructura como código (IAC) es una de las caracteristicas de DevOps que se refiere a la capacidad de administrar configurar y aprovisionar infraestructura automáticamente. IAC es un enfoque de definición de infraestructura y componentes de red a través de código descriptivo o de alto nivel; es decir, infraestructura programable. Varias herramientas como Vagrant, Ansible, Docker, Chef, Terraform, Puppet y más, en forma independientemente o combinadas, facilitan la vida al automatizar el aprovisionamiento de la infraestructura y la automatización del despliegue.

**Terraform** es una de esas herramientas. Acá hay un ejemplo donde usaremos Terraform para aprovisionar una infraestructura completa en Azure. Crearemos todos los componentes desde cero, incluidos el grupo de recursos, la red virtual, la subred, la NIC, los grupos de seguridad, las máquinas virtuales, etc.

Para revisar el detalle de los valores usados para la configuraci&oacute;n revisar el archivo *variables.tf*

Este proyecto permite crear un grupo de recursos en Azure (POC-Terraform) con todos los recursos necesarios para montar un Apache con PHP y una BD de ejemplo. Se debe tener una cuenta con privilegios de administraci&oacute;n.

Para instalar Terraform descargarlo [ac&aacute;] y las instrucciones de [instalaci&oacute;n] 

En la carpeta config estan los archivos de configuración de Terraform:

<ul>
	<li><b>dns_zone.tf:</b> Zona DNS dentro de Azure</li>
	<li><b>networks.tf:</b> Redes y subredes</li>
	<li><b>public_ip_nic.tf:</b> IP Pública fija de acceso a las VM</li>
	<li><b>security_group.tf:</b> Reglas de acceso para cada una de las VM</li>
	<li><b>storage.tf:</b> ALmacenamiento para cada VM</li>
	<li><b>terraform.tfvars:</b> Valores de variables de configuraci&oacute;n</li>
	<li><b>variables.tf:</b> Definici&oacute;n de las variables</li>
	<li><b>vm_ext.tf:</b> Definici&oacute;n de las extensiones necesarias para ejecutarse en las VM una vez provisionadas</li>
	<li><b>vm_prop.tf:</b> Definici&oacute;n de las VM's</li>	
</ul>

Para poder configurar los valores de identidad de la suscripción de Azure se deben tener los siguientes datos:

<ul>
	<li>tenant_id</li>
	<li>subscription_id</li>
	<li>client_id</li>
	<li>client_secret</li>
</ul>

En la siguiente imagen se muestra como rescatar el Id del Tenant, que corresponde al ID del Azure Active Directory o del Tenant<br>
![Id Tenant](https://axitydevstorage.blob.core.windows.net/images/tenant_id.png)

En la siguiente imagen se muestra como rescatar el Id de la Suscripci&oacute;n,<br>
![Id Suscripcion](https://axitydevstorage.blob.core.windows.net/images/subscription_id.png)

El proceso para incorporar el client_id y el client_secret es un poco m&aacute;s complejo y consta de los siguientes pasos:

1. Crear una aplicaci&oacute;n nueva para que Terraform pueda interactuar con Azure<br>
![New App](https://axitydevstorage.blob.core.windows.net/images/new_app.png)<br>
![New App](https://axitydevstorage.blob.core.windows.net/images/new_app_2.png)<br>


2. Desde [Azure CLI] se debe configurar el acceso por roles para esta aplicaci&oacute;n reci&eacute;n creada<br>
![RBAC](https://axitydevstorage.blob.core.windows.net/images/rbac.png)
  

Todos los valores se pueden modificar en el archivo de variables terraform.tfvars

Para ejecutar la configuraci&oacute;n se debe ejecutar:

```sh
>terraform apply
```

Para eliminar la configuraci&oacute;n se debe ejecutar:

```sh
>terraform destroy
```

**@TODO** La configuraci&oacute;n de las extensiones no está funcionando, se deben revisar los scripts

[//]: #Links
[Azure CLI]: <https://shell.azure.com>
[ac&aacute;]: <https://www.terraform.io/downloads.html>
[instalaci&oacute;n]: <https://www.terraform.io/intro/getting-started/install.html>