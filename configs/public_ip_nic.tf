resource "azurerm_public_ip" "ai_pip" {
  name 				= "AI-Terraform-PIP"
  location 			= "${var.location}"
  resource_group_name 		= "${azurerm_resource_group.terraform_rg.name}"
  public_ip_address_allocation 	= "static"

  tags {
    group = "${var.group_name}"
  }
}

resource "azurerm_network_interface" "public_nic" {
  name 		      = "AI-Terraform-Web"
  location 	      = "${var.location}"
  resource_group_name = "${azurerm_resource_group.terraform_rg.name}"
  network_security_group_id = "${azurerm_network_security_group.nsg_web.id}"

  ip_configuration {
    name 			= "AI-Terraform-WebPrivate"
    subnet_id 			= "${azurerm_subnet.ai_subnet_1.id}"
    private_ip_address_allocation = "dynamic"
    public_ip_address_id	= "${azurerm_public_ip.ai_pip.id}"
  }
  tags {
    group = "${var.group_name}"
  }
}

resource "azurerm_public_ip" "ai_db_pip" {
  name                  = "AI-Terraform-DB-PIP"
  location              = "${var.location}"
  resource_group_name   = "${azurerm_resource_group.terraform_rg.name}"
  public_ip_address_allocation = "static"

  tags {
    group = "${var.group_name}"
  }
}

resource "azurerm_network_interface" "private_nic" {
  name 			= "AI-Terraform-DB"
  location 		= "${var.location}"
  resource_group_name 	= "${azurerm_resource_group.terraform_rg.name}"
  network_security_group_id = "${azurerm_network_security_group.terraform_nsg_db.id}"

  ip_configuration {
    name 			= "AI-Terraform-DBPrivate"
    subnet_id 			= "${azurerm_subnet.ai_subnet_2.id}"
    private_ip_address_allocation = "static"
    private_ip_address = "192.168.2.5"
    public_ip_address_id        = "${azurerm_public_ip.ai_db_pip.id}"
  }
  tags {
    group = "${var.group_name}"
  }
}