resource "azurerm_dns_zone" "ai_dns" {
  name                  = "${var.domain}"
  resource_group_name   = "${azurerm_resource_group.terraform_rg.name}"
}

resource "azurerm_dns_a_record" "ai_a" {
  name      = "AI-A_Record"
  zone_name = "${azurerm_dns_zone.ai_dns.name}"
  resource_group_name   = "${azurerm_resource_group.terraform_rg.name}"
  ttl = 300
  records = ["${azurerm_public_ip.ai_pip.ip_address}"]
}